<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
//$GLOBALS['APPLICATION']->RestartBuffer();
CJSCore::Init(array('ajax'));
if (isset($_POST["PARAM"])){
	$id = htmlspecialchars($_POST["PARAM"]);
	$id_process = explode("_",$id);
	$id = $id_process[1];
$dbBasketItems = CSaleBasket::GetList(
        array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
        array(
                "LID" => SITE_ID
            ),
        false,
        false,
        array("ID",
              "PRODUCT_ID")
    );
while ($arItems = $dbBasketItems->Fetch())
{
	if ($arItems["PRODUCT_ID"] == $id) CSaleBasket::Delete($arItems["ID"]);
}
}
//echo CUtil::PhpToJSObject($JSONResult);
//echo $id;
?>
