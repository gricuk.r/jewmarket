<? use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
/** @var array $arResult */

?>
<script>
    BX = top.BX;
    $ = top.$;
</script>
<div class="popup-wrap">
    <? if ($arResult["SUCCESS_ADD"] != "Y"): ?>
        <header class="popup-header"><?= Loc::getMessage("ONE_CLICK_BUY_POPUP_TITLE") ?></header>
    <? endif ?>
    <? if (isset($arResult["ORDER"])): ?>
        <?
        /** @var Bitrix\Sale\Order $order */
        $order = $arResult["ORDER"];
        ?>
        <header class="popup-header">
            Заказ # <?= $order->getId() ?>
            от <?= $order->getDateInsert()->format("d.m.Y") ?></header>
    <? endif ?>
    <div class="popup-content">
        <div class="popup-form form">
            <? if (isset($arResult["MESSAGES"]) && ($arResult["SUCCESS_ADD"] != "Y")): ?>
                <? foreach ($arResult["MESSAGES"] as $message): ?>
                    <? ShowMessage($message) ?>
                <? endforeach ?>
            <? endif ?>
            <? if (($arResult["SUCCESS_ADD"] != "Y") && !($arResult["DONT_SHOW_FORM"] === true)): ?>
                <form name="oneClickBuyForm" action="<?= $arResult["FORM"]["ACTION"] ?>" method="POST">
                    <input type="hidden" value="addOrder" name="action">
                    <? foreach ($arResult["FIELDS"] as $code => $field): ?>

                        <div class="form-group">
                            <label>
                                <?= $field["NAME"] ?>
                                <? if ($field["REQUIRED"]): ?>
                                    <span class="starrequired">*</span>
                                <? endif ?>
                            </label>

                            <input type="text" name="<?= $code ?>"
                                   <? if ($field["REQUIRED"]): ?>required=""<? endif ?>
                                   <? if ($field["VALUE"]): ?>value="<?= $field["VALUE"] ?>"<? endif ?>
                                   class="form-control">
                        </div>
                    <? endforeach ?>
                    <!--            <input type="hidden" name="form_hidden_9" value="722">-->


                    <div class="form-group">
                        <button type="submit" name="button"
                                class="btn btn-primary">
                            <?= Loc::getMessage("ONE_CLICK_BUY_POPUP_ORDER_BTN") ?>
                        </button>
                    </div>
                </form>
                <script type="text/javascript">

                    function refreshInputs() {
                        $("input[name=PERSONAL_PHONE]").mask("+7 999 999 99 99");

                        var inputs = $(".form-row .js-inp-tip input");
                        for (i = 0; i < inputs.length; i++) {
                            if ($(inputs[i]).val() !== "") {
                                $(inputs[i]).trigger("focusin");
                            }
                        }
                    }

                    setTimeout($.proxy(refreshInputs, top), 500);
                </script>
            <? else: ?>
                <div class="form-row">
                    <div style="max-width: 300px; min-width: 230px;">
                        Наши менеджеры свяжутся с Вами для уточнения удобных для Вас способов оплаты и доставки.
                        Вы можете следить за выполнением своего заказа в
                        <a class="link-green" href="/personal/">персональном разделе сайта</a>
                    </div>
                    <div class="form-row__btn text-center">
                        <button onclick="$.fancybox.close(); return false;"
                                name="button"
                                class="btn-green size-l btn btn-submit">
                            <?= Loc::getMessage("POPUP_CLOSE_BTN") ?>
                        </button>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).trigger("resize");
                </script>
            <? endif ?>
        </div>
    </div>
</div>
