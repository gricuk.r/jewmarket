<?

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Sale;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class OneClickBuy extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $request = Application::getInstance()->getContext()->getRequest();

        $arParams["FIELDS"] = array(
            "NAME" => array(
                "REQUIRED" => true,
                "NAME" => "Имя",
                "VALIDATOR" => "validName",
                "FROM_USER_PROFILE" => "Y"
            ),
            "PERSONAL_PHONE" => array(
                "REQUIRED" => true,
                "NAME" => "Телефон",
                "VALIDATOR" => "validPhone",
                "FROM_USER_PROFILE" => "Y",
                "DESCRIPTION" => "Мы свяжемся с Вами по указанному номеру."
            ),

            "EMAIL" => array(
                "REQUIRED" => true,
                "NAME" => "Email",
                "VALIDATOR" => "validEmail",
                "FROM_USER_PROFILE" => "Y",
                "DESCRIPTION" => "Мы свяжемся с Вами по указанному номеру."
            ),
        );

        return $arParams;
    }

    public function executeComponent()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $product = $request->get("product_id");
        $action = $request->get("action");

        $this->arResult["FORM"]["ACTION"] = POST_FORM_ACTION_URI;
        $this->arResult["FIELDS"] = $this->arParams["FIELDS"];
        $this->arResult["USER"] = $this->getUser();

        if ((intval($this->arResult["USER"]["ID"]) > 0) && (($countExistOrders = $this->getCountExistOrders()) > $this->arParams["MAX_ORDER_COUNT"])) {
            $this->arResult["MESSAGES"][] = array(
                "TYPE" => "ERROR",
                "MESSAGE" => "Вами оформлено {$countExistOrders} заказов  в 1 клик, дождите окончания обработки всех текущих заказов или пройдите полную процедуру оформления закза"
            );

            $this->arResult["DONT_SHOW_FORM"] = true;
        }

        if (empty($this->arResult["MESSAGES"])) {
            if ($product != "") {
                if ($action != "addOrder") {
                    if ($this->arResult["USER"]) {
                        foreach ($this->arResult["FIELDS"] as $code => $field) {
                            if (($field["FROM_USER_PROFILE"] == "Y") && (isset($this->arResult["USER"][$code]))) {
                                $this->arResult["FIELDS"][$code]["VALUE"] = $this->arResult["USER"][$code];
                            }
                        }
                    }
                } else {
                    $allFieldValid = true;
                    foreach ($this->arParams["FIELDS"] as $code => $field) {
                        $fieldValue = $request->getPost($code);
                        $this->arResult["FIELDS"][$code]["VALUE"] = $fieldValue;
                        $isValid = ($fieldValue != "") || ($field["REQUIRED"] != true);

                        if (isset($field["VALIDATOR"])) {
                            $isValid &= call_user_func(array($this, $field["VALIDATOR"]), $fieldValue);
                        }

                        //Если поле не пустое, или не должно быть заполнено и если задан валидатор, то валидируем поле
                        if ($isValid && isset($field["VALIDATOR"])) {
//                            \Bitrix\Main\Diag\Debug::dump("isValid");
                            $isValid &= call_user_func(array($this, $field["VALIDATOR"]), $fieldValue);
                        }

                        if (!$isValid) {
                            $allFieldValid = false;
                            $this->arResult["MESSAGES"][] = array(
                                "TYPE" => "ERROR",
                                "MESSAGE" => "Не верный формат поля " . $field["NAME"]
                            );;
                        }

                    }
                    if ($allFieldValid) {
                        $this->addOrder($product);
                    }

                }
            } else {
                $this->arResult["MESSAGES"][] = array(
                    "TYPE" => "ERROR",
                    "MESSAGE" => "Не передан ID товара"
                );;
            }
        }


        $this->includeComponentTemplate();
    }

    private function getCountExistOrders()
    {
        Loader::includeModule('sale');
        $dbOrders = \Bitrix\Sale\Order::getList(array(
            "select" => array("*"),
            "filter" => array(
                "USER_ID" => $this->arResult["USER"]["ID"],
                "STATUS_ID" => $this->arParams["STATUS_ID"]
            )
        ));


        return $dbOrders->getSelectedRowsCount();
    }

    private function addOrder($product)
    {
        Loader::includeModule('sale');
        Loader::includeModule('catalog');

        if (!$this->arResult["USER"]) {
            $email = $this->arResult["FIELDS"]["EMAIL"]["VALUE"];
            $dbUser = \Bitrix\Main\UserTable::getList(array(
                "select" => array("*"),
                "filter" => array(
                    "EMAIL" => $this->arResult["FIELDS"]["EMAIL"]["VALUE"]
                ),
            ));
            if ($arUser = $dbUser->fetch()) {
                $this->arResult["USER"] = $arUser;
            } else {
                $user = new CUser;
                $PASSWORD_LENGTH = 8;
                $pass = substr(str_shuffle(strtolower(sha1(rand() . time() . "Lorem i1psum dolor sit amet, consectetur 9adipiscing elit8, sed do eiusmod tempor incididunt ut labosr3e et dolore magna aliqua. Ut enim ad minims ve2niam, quis n678osdtrud exercitation 456ulla2mco laboris nisi ut aliquip ex ea commodo con7sequat. Dauis aute irure dolo3r in raeprehenderit in volupta4te velit esse cil5aslum dolor4e eu fugiat naulla pariatur. Excepteur sint occaecat cupidatat no4n proi1dent, sunt in culpa qui o9fficia deseru0nt mollit anim id est laborum."))), 0, $PASSWORD_LENGTH);

                $arFields = Array(
                    "NAME" => $this->arResult["FIELDS"]["NAME"]["VALUE"],
                    "EMAIL" => $email,
                    "LOGIN" => $email,
                    "LID" => SID,
                    "ACTIVE" => "Y",
                    "GROUP_ID" => $this->arParams["USER_GROUP_ID"],
                    "PASSWORD" => $pass,
                    "CONFIRM_PASSWORD" => $pass,
                    "PERSONAL_PHONE" => $this->arResult["FIELDS"]["PERSONAL_PHONE"]["VALUE"]
                );

                $ID = $user->Add($arFields);
                if (intval($ID) > 0) {
                    $arFields["ID"] = $ID;
                    $this->arResult["USER"] = $arFields;
                } else {
                    $this->arResult["MESSAGES"][] = array(
                        "TYPE" => "ERROR",
                        "MESSAGE" => "Ошибка при создании пользователя: " . $user->LAST_ERROR
                    );
                }
            }
        }

        if ((intval($this->arResult["USER"]["ID"]) > 0) && (($countExistOrders = $this->getCountExistOrders()) > $this->arParams["MAX_ORDER_COUNT"])) {
            $this->arResult["MESSAGES"][] = array(
                "TYPE" => "ERROR",
                "MESSAGE" => "Вами оформлено {$countExistOrders} заказов  в 1 клик, дождите окончания обработки всех текущих заказов или пройдите полную процедуру оформления закза"
            );

            $this->arResult["DONT_SHOW_FORM"] = true;
        } elseif (intval($this->arResult["USER"]["ID"]) > 0) {
            $siteId = Bitrix\Main\Context::getCurrent()->getSite();

            //////корзина
            $basket = Sale\Basket::create($siteId);
            $item = $basket->createItem('catalog', $product);

            $item->setFields(array(
                'QUANTITY' => 1,
                'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
                'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
            ));

            $arProduct = \Bitrix\Catalog\ProductTable::getById($product)->fetch();
            if ($arProduct["TYPE"] == \Bitrix\Catalog\ProductTable::TYPE_OFFER) {
                $productProperties = [];
                $parentProduct = \CCatalogSku::GetProductInfo($product);
                $productProperties = CIBlockPriceTools::GetOfferProperties(
                    $product,
                    $parentProduct["IBLOCK_ID"],
                    $this->arParams["OFFERS_CART_PROPERTIES"],
                    []
                );
                if (!empty($productProperties)) {
                    $basketItemPropertyCollection = $item->getPropertyCollection();
                    $basketItemPropertyCollection->setProperty($productProperties);
                }
            }


            $basket->save();


            ////////заказ
            $order = Bitrix\Sale\Order::create($siteId, $this->arResult["USER"]["ID"]);
            $order->setPersonTypeId(1);
            $order->setBasket($basket);
            $order->setField("STATUS_ID", $this->arParams["STATUS_ID"]);

            $orderProps = $order->getPropertyCollection();
            if ($phoneProp = $orderProps->getPhone()) {
                $phoneProp->setValue($this->arResult["FIELDS"]["PERSONAL_PHONE"]["VALUE"]);
            }
            if ($emailProp = $orderProps->getUserEmail()) {
                $emailProp->setValue($this->arResult["FIELDS"]["EMAIL"]["VALUE"]);
            }

            /////////оплата

            $shipmentCollection = $order->getShipmentCollection();
            $shipment = $shipmentCollection->createItem(
                Bitrix\Sale\Delivery\Services\Manager::getObjectById($this->arParams["DELIVERY"])
            );

            $shipmentItemCollection = $shipment->getShipmentItemCollection();

            foreach ($basket as $basketItem) {
                $item = $shipmentItemCollection->createItem($basketItem);
                $item->setQuantity($basketItem->getQuantity());
            }

            /////////////отгрузка
            $paymentCollection = $order->getPaymentCollection();
            $payment = $paymentCollection->createItem(
                Bitrix\Sale\PaySystem\Manager::getObjectById($this->arParams["PAY_SYSTEM"])
            );

            /** @var Bitrix\Sale\Payment $payment */
            $payment->setField("SUM", $order->getPrice());
            $payment->setField("CURRENCY", $order->getCurrency());


            $result = $order->save();
            if (!$result->isSuccess()) {
                \Bitrix\Main\Diag\Debug::dump($result->getErrors());
            }

            if ($result->isSuccess()) {
                $this->arResult["ORDER"] = $order;
                $this->arResult["SUCCESS_ADD"] = "Y";
                $this->arResult["MESSAGES"][] = array(
                    "TYPE" => "OK",
                    "MESSAGE" => isset($this->arParams["OK_MESSAGE"]) ? $this->arParams["OK_MESSAGE"] : "Заказ успешно оформлен"
                );
            } else {
                $this->arResult["MESSAGES"][] = array(
                    "TYPE" => "ERROR",
                    "MESSAGE" => "Ошибка при создании заказа: " . $result->getErrors()
                );
            }
        }
    }

    private function AddOrderProperty($code, $value, $order)
    {
        if (!strlen($code)) {
            return false;
        }
        if (CModule::IncludeModule('sale')) {
            if ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => $code))->Fetch()) {
                return CSaleOrderPropsValue::Add(array(
                    'NAME' => $arProp['NAME'],
                    'CODE' => $arProp['CODE'],
                    'ORDER_PROPS_ID' => $arProp['ID'],
                    'ORDER_ID' => $order,
                    'VALUE' => $value,
                ));
            }
        }
    }

    /** Получение информации о текущем пользователе.
     * @return array
     */
    private function getUser()
    {
        global $USER;
        return CUser::GetByID($USER->GetID())->Fetch();
    }

    /*
     * Валидатор телефона
     *
     * @return bool
     */
    private function validPhone($phone)
    {
        $re = '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?([\d\- ]{7,10})$/';
        $str = $phone;
        preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
        return count($matches) > 0;
    }

    /*
     * Валидатор email
     *
     * @return bool
     */
    private function validEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    /*
     * Валидатор имени
     *
     * @return bool
     */
    private function validName($name)
    {
        return (bool)preg_match('/^[a-zA-Zа-яА-Я ]+$/ui', $name);
    }
}