<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Гарантия");
$APPLICATION->SetTitle("Гарантия");
?><p>
	<b>Для осуществления гарантийного обслуживания необходимы:</b>
</p>
<ul>
	<li>правильно и без помарок и исправлений заполненный гарантийный талон, в котором должны быть указаны артикул и штрих-код изделия, дата продажи;</li>
	<li>документ, подтверждающий покупку (чек, накладная); </li>
	<li>полная комплектация товара.</li>
</ul>
<p>
	Обращаем также ваше внимание на то, что при получении и оплате заказа покупатель в присутствии курьера обязан проверить комплектацию и внешний вид изделия на предмет отсутствия физических дефектов (царапин, трещин, сколов и т.п.) и полноту комплектации. После отъезда курьера претензии по этим вопросам не принимаются.
</p>
 <b>Гарантийное обслуживание не производится, если:</b>
<ul>
	<li>изделие имеет следы механического повреждения или вскрытия </li>
	<li>нарушены заводские пломбы </li>
	<li>были нарушены условия эксплуатации, транспортировки или хранения </li>
	<li>проводился ремонт лицами, не являющимися сотрудниками авторизованного сервисного центра</li>
</ul>
<p>
	Подробное описание условий предоставления гарантии вы можете найти на нашем сайте.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>