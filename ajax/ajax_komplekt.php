<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_STATISTIC', true);
define('NO_AGENT_CHECK', true);
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

if(!isset($_POST['doaction'])||!$_POST['doaction']) die('Wrong parameters');
//require($_SERVER["DOCUMENT_ROOT"]."/ajax/CKomplekt.php");

switch($_POST['doaction']){

	case 'add':
		$elementsIds = $_POST['values'];
		$komplekt_tip = $_POST['komplekt_tip'];

		if(count($elementsIds)<2) die();

		$arRes = XFive\Catalog\CKomplekt::getOrAddKomplekt($USER->GetID(),$komplekt_tip, $elementsIds);
		$APPLICATION->RestartBuffer();
		if($arRes === false) die(json_encode(array("status"=>"fail","text"=>CKomplekt::$error_text)));

		die(json_encode(array("status"=>"ok","id"=>$arRes['KOMPLEKT']['ID'],"detail_url"=>$arRes['KOMPLEKT']['DETAIL_PAGE_URL'])));

	case 'changebasketkomplekt':

		$basketId = $_POST['basket_id'];
		$arOffersId = $_POST['values'];

		if(count($arOffersId)<2) die(json_encode($arOffersId));
		$arRes = XFive\Catalog\CKomplekt::changeKomplektForBasketId($USER->GetID(),$basketId,$arOffersId);
		$APPLICATION->RestartBuffer();
		if($arKomplekt === false) die(json_encode(array("status"=>"fail","text"=>CKomplekt::$error_text)));

		$PRICE = \CCurrencyLang::CurrencyFormat($arRes['PRICE']['PRICE'], "RUB");
		$SUMMA = \CCurrencyLang::CurrencyFormat($arRes['SUMMA'], "RUB");
		die(json_encode(array("status"=>"ok","NAME"=>$arRes['KOMPLEKT']['NAME'],"DETAIL_PAGE_URL"=>$arRes['KOMPLEKT']['DETAIL_PAGE_URL'],'PRICE'=>$PRICE,'SUMMA'=>$SUMMA)));

	default:
		die('Wrong parameters');

}