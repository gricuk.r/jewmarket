<?
require_once "include/const.php";
require_once "vendor/autoload.php";


$eventManager = new XFive\Main\EventManager();
$eventManager->initEvents();


function p($data, $ignore = false){
    global $USER;

    if ($USER->IsAdmin() || $ignore) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}

