<?php
/**
 * Created by PhpStorm.
 * User: Winer
 * Date: 21.07.2017
 * Time: 8:56
 */

namespace XFive\Sale;


use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;

class Import1C
{
    public function OnSuccessCatalogImport1C($arParams, $arFields)
    {
        $Props = new \XFive\Iblock\Properties();

        $Props->propSaver();

        // Обновление символьных кодов элементов
        Loader::includeModule('iblock');

        $arSelect = Array('ID', 'NAME', 'PROPERTY_CML2_ARTICLE');
        $arFilter = Array('IBLOCK_ID' => IBLOCK_CATALOG_ID);
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

        while ($ob = $res->Fetch()) {
            $elem = new \CIBlockElement();
            $elem->Update(
                $ob['ID'],
                array(
                    'CODE' => \CUtil::translit(
                        $ob['NAME'] . '-' . $ob['PROPERTY_CML2_ARTICLE_VALUE'],
                        LANGUAGE_ID,
                        array("replace_space" => "-", "replace_other" => "-")
                    )
                )
            );
        }
    }
}