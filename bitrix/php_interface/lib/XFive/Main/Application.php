<?php
/**
 * Created by PhpStorm.
 * User: Winer
 * Date: 12.07.2017
 * Time: 9:23
 */

namespace XFive\Main;


use XFive\Patterns\Singleton;

class Application
{
    use Singleton;

    /**
     * @return \XFive\Main\Settings
     */
    public function getSettings()
    {
        return \XFive\Main\Settings::getInstance();
    }

    public function initSettings()
    {
        $application = self::getInstance();

        /** @var \XFive\Main\Settings $settings */
        $settings = $application->getSettings();

        $settings->set(//Цены в настройках компонентов
            "CATALOG_PRICES",
            array(
                "Розничная"
            )
        );

        $settings->set(//Свойства ТП добавляемых в корзину
            "OFFERS_CART_PROPERTIES",
            array("VSTAVKA", "TSVET_VSTAVKI", "RAZMER", "DLINA")
        );

        $settings->set(
            "OFFERS_PROPERTY_CODE",
            array("VSTAVKA",  "RAZMER", "DLINA", PROPERTY_ARTNUMBER_CODE, "SREDNIY_VES")
        );

        $settings->set(
            "OFFER_TREE_PROPS",
            array("VSTAVKA", "TSVET_VSTAVKI", "RAZMER", "DLINA")
        );

        $settings->set(
            "FILTER_OFFERS_PROPERTY_CODE",
            array("CML2_ARTICLE", "VSTAVKA", "TSVET_VSTAVKI", "RAZMER", "DLINA")
        );

        $settings->set(
            "COMPARE_OFFERS_PROPERTY_CODE",
            array("MORE_PHOTO", "CML2_ARTICLE", "VSTAVKA", "TSVET_VSTAVKI", "RAZMER", "DLINA")
        );



        $settings->set(
            "PROPERTY_CODE_OFFERS",
            array("CML2_LINK", "CML2_ARTICLE", "VSTAVKA", "TSVET_VSTAVKI", "RAZMER", "DLINA")
        );

    }

    public function onPageStart()
    {
        self::initSettings();
    }
}