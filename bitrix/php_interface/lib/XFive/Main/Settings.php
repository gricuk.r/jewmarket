<?php

namespace XFive\Main;


use XFive\Patterns\Singleton;

class Settings
{
    use Singleton;

    private $settings = array();

    public function set($key, $value)
    {
        $this->settings[$key] = $value;
    }

    public function get($key)
    {
        return $this->settings[$key];
    }
}