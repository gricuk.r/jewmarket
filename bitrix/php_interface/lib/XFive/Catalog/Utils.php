<?php
/**
 * Created by PhpStorm.
 * User: Winer
 * Date: 06.09.2017
 * Time: 13:15
 */

namespace XFive\Catalog;


use Bitrix\Main\Loader;

class Utils
{
    public static function getPicturesForOffersPropFromOffers($arProperty, $arOffers, $arEmptyPreview)
    {
        $result = $arProperty;
        $result["SHOW_MODE"] = "PICT";
        foreach ($result["VALUES"] as $code => $value) {
            foreach ($arOffers as $offer) {
                $productId = $offer["PROPERTIES"]["CML2_LINK"]["VALUE"];

                if (
                    ($offer["PROPERTIES"][$arProperty["CODE"]]["VALUE_ENUM_ID"] == $code)
                    && ($offer["PREVIEW_PICTURE"] || $offer["DETAIL_PICTURE"])
                ) {
                    $picture = $offer["DETAIL_PICTURE"] ? $offer["DETAIL_PICTURE"] : $offer["PREVIEW_PICTURE"];
                    $pictureForProp = \CFile::ResizeImageGet(
                        $picture,
                        array(
                            'width' => 50,
                            'height' => 50
                        ),
                        BX_RESIZE_IMAGE_PROPORTIONAL,
                        true
                    );

                    $pictureForGrid = \CFile::ResizeImageGet(
                        $picture,
                        array(
                            'width' => 222,
                            'height' => 222
                        ),
                        BX_RESIZE_IMAGE_PROPORTIONAL,
                        true
                    );

                    $pictureForProp = array_change_key_case($pictureForProp, CASE_UPPER);
                    $pictureForGrid = array_change_key_case($pictureForGrid, CASE_UPPER);

                    $result["VALUES"][$code]["PICT"] = $pictureForProp;
                    $result["VALUES"][$code]["PICT2"] = $pictureForProp;
                    $result["VALUES"][$code]["PICT3"] = $pictureForGrid;
//                    if (isset($result["VALUES"][$code]["MAP_BY_ID"][$productId])) {
//                        continue;
//                    }

                    $result["VALUES"][$code]["MAP_BY_ID"][$productId]["THUMB"] = $pictureForProp;
                    $result["VALUES"][$code]["MAP_BY_ID"][$productId]["PICTURE"] = $pictureForGrid;
                    $result["VALUES"][$code]["MAP_BY_ID"][$productId]["NAME"] = $offer["NAME"];
                    $result["VALUES"][$code]["PRODUCT_NAME"] = $offer["NAME"];
//                    break;
                } else if ($offer["PROPERTIES"][$arProperty["CODE"]]["VALUE_ENUM_ID"] == $code) {
                    $result["VALUES"][$code]["MAP_BY_ID"][$productId]["THUMB"] = $arEmptyPreview;
                    $result["VALUES"][$code]["MAP_BY_ID"][$productId]["PICTURE"] = $arEmptyPreview;
                    $result["VALUES"][$code]["MAP_BY_ID"][$productId]["NAME"] = $offer["NAME"];
                    $result["VALUES"][$code]["PRODUCT_NAME"] = $offer["NAME"];
                }


            }
            if (!$result["VALUES"][$code]["PICT"]) {
                $result["VALUES"][$code]["PICT"] = $arEmptyPreview;
                $result["VALUES"][$code]["PICT2"] = $arEmptyPreview;
                $result["VALUES"][$code]["PICT3"] = $arEmptyPreview;


            }
        }


        return $result;
    }

    public static function getHowToLinkBySectionId($sectionId)
    {
        Loader::includeModule("iblock");
        $result = false;
        if ($sectionId > 0) {
            $dbNav = \CIBlockSection::GetNavChain(false, $sectionId);
            $sections = array();
            $iblockId = 0;
            while ($section = $dbNav->Fetch()) {
                $sections[$section["ID"]] = $section;
                $iblockId = $section["IBLOCK_ID"];
            }

            $arFilter = array(
                "ID" => array_keys($sections),
                "IBLOCK_ID" => $iblockId
            );

            $rsSect = \CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, false, array("ID", "IBLOCK_ID", "NAME", "UF_HOW_TO_GET_SIZE"));
            while ($arSect = $rsSect->GetNext()) {
                $sections[$arSect["ID"]]["UF_HOW_TO_GET_SIZE"] = $arSect["UF_HOW_TO_GET_SIZE"];
            }

            foreach ($sections as $section) {
                if (strlen($section["UF_HOW_TO_GET_SIZE"]) > 0) {
                    $result = $section["UF_HOW_TO_GET_SIZE"];
                }
            }
        }
        return $result;
    }

    public static function getRecommendationByOfferId($offerId)
    {
        $result = array();
        Loader::includeModule("iblock");
        Loader::includeModule("sale");

        $recommendationProperty = "VSTAVKA";
        $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_{$recommendationProperty}", "PROPERTY_CML2_LINK");
        $arFilter = Array(
            "ID" => $offerId
        );

        $dbOffer = \CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arSelect);
        if ($arOffer = $dbOffer->Fetch()) {
            $result["RECOMMENDATION_PROPERTY"] = array(
                "CODE" => $recommendationProperty,
                "VALUE" => $arOffer["PROPERTY_{$recommendationProperty}_ENUM_ID"]
            );

            $ibRecommendationProperty = \CIBlockProperty::GetList(["SORT" => "ASC"], [
                "IBLOCK_ID" => $arOffer["IBLOCK_ID"],
                "CODE" => $recommendationProperty,
            ])->Fetch();
            $result["RECOMMENDATION_PROPERTY"]["IBLOCK_CONF"] = $ibRecommendationProperty;

            $arSelect = Array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID");
            $arFilter = Array(
                "ID" => $arOffer["PROPERTY_CML2_LINK_VALUE"]
            );

            $resProduct = \CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arSelect);
            if ($arProduct = $resProduct->Fetch()) {
                $result["SECTION_ID"] = $arProduct["IBLOCK_SECTION_ID"];
            }


            $arSelect = Array("IBLOCK_ID", "PROPERTY_CML2_LINK");
            $arFilter = Array(
                "IBLOCK_ID" => $arOffer["IBLOCK_ID"],
                "ACTIVE" => "Y",
                "PROPERTY_{$recommendationProperty}" => $arOffer["PROPERTY_{$recommendationProperty}_ENUM_ID"],
                "!ID" => $offerId,
                "!PROPERTY_CML2_LINK_VALUE" => $arOffer["PROPERTY_CML2_LINK_VALUE"]
            );

            $res = \CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arSelect);
            while ($arRecommendedOffer = $res->Fetch()) {
                if (!isset($result["PRODUCTS"][$arRecommendedOffer["PROPERTY_CML2_LINK_VALUE"]])) {
                    $result["PRODUCTS"][$arRecommendedOffer["PROPERTY_CML2_LINK_VALUE"]] = array(
                        "COUNT" => 1,
                        "ID" => $arRecommendedOffer["PROPERTY_CML2_LINK_VALUE"],
                    );
                } else {
                    $result["PRODUCTS"][$arRecommendedOffer["PROPERTY_CML2_LINK_VALUE"]]["COUNT"]++;
                }
                $result["PRODUCTS_IDS"][$arRecommendedOffer["PROPERTY_CML2_LINK_VALUE"]] = $arRecommendedOffer["PROPERTY_CML2_LINK_VALUE"];
            }


            $sortCallback = function ($a, $b) {
                return $b["COUNT"] - $a["COUNT"];
            };

            usort($result["PRODUCTS"], $sortCallback);


        }


        return $result;
    }
}