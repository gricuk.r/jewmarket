<?php
/**
 * Created by PhpStorm.
 * User: Winer
 * Date: 28.08.2018
 * Time: 11:06
 */

namespace XFive\Catalog;


class Product
{
    function UpdateProductQuantity($id, $arFields)
    {
        $quantity = $arFields['QUANTITY'];

        if (\CModule::IncludeModule('catalog') && \CModule::IncludeModule('iblock')) {
            $arProductInfo = \CCatalogSKU::GetProductInfo($id);
            if (is_array($arProductInfo)) {
                $arOffersInfo = \CCatalogSKU::GetInfoByProductIBlock($arProductInfo['IBLOCK_ID']);
                $arFilter = array(
                    'IBLOCK_ID' => IBLOCK_OFFERS_ID,
                    "PROPERTY_CML2_LINK" => $arProductInfo['ID'],
                    "!ID" => $id,
                );

                $obOffersList = \CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, false, array("CATALOG_QUANTITY"));
                while ($arOffers = $obOffersList->Fetch()) {
                    $quantity += $arOffers["CATALOG_QUANTITY"];
                }

                $arFieldsProduct = array(
                    "QUANTITY" => $quantity,
                );
                \CCatalogProduct::Update($arProductInfo['ID'], $arFieldsProduct);
            }
        }
    }
}