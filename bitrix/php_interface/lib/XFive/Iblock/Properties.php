<?php
/**
 * Created by PhpStorm.
 * User: Winer
 * Date: 21.07.2017
 * Time: 9:05
 */

namespace XFive\Iblock;


use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;

class Properties
{
    public function onPropChange(Entity\Event $event)
    {
        $result = new Entity\EventResult;
//        $data = $event->getParameter("fields");
        \Bitrix\Main\Diag\Debug::writeToFile($result);
    }

    public function propSaver()
    {
        if (Loader::includeModule("iblock")) {
            $propConfig = array(
                "SPECIALOFFER" => array(
                    "NAME" => "Спецпредложени"
                ),
                "HIT" => array(
                    "NAME" => "Хит"
                ),
                "NEWPRODUCT" => array(
                    "NAME" => "Новинка"
                ),
            );

            $dbProp = PropertyTable::getList(array(
                "select" => array("*"),
                "filter" => array(
                    "NAME" => array_keys($propConfig),
                    "CODE" => array_keys($propConfig),
                )
            ));

            while ($prop = $dbProp->fetch()) {
                PropertyTable::update(
                    $prop["ID"],
                    array(
                        "NAME" => $propConfig[$prop["CODE"]]["NAME"]
                    )
                );
            }

        }
    }
}