<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
if (!empty($arResult['ITEMS']))
{
	

	$templateLibrary = array('popup');
	$currencyList = '';
	if (!empty($arResult['CURRENCIES']))
	{
		$templateLibrary[] = 'currency';
		$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
	}
	$templateData = array(
		'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
		'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
		'TEMPLATE_LIBRARY' => $templateLibrary,
		'CURRENCIES' => $currencyList
	);
	unset($currencyList, $templateLibrary);

	$arSkuTemplate = array();
	if (!empty($arResult['SKU_PROPS']))
	{
		
		foreach ($arResult['SKU_PROPS'] as &$arProp)
		{
			$templateRow = '';
			if ('TEXT' == $arProp['SHOW_MODE'])
			{
				if (5 < $arProp['VALUES_COUNT'])
				{
					$strClass = 'bx_item_detail_size full';
					$strWidth = ($arProp['VALUES_COUNT']*20).'%';
					$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
					$strSlideStyle = '';
				}
				else
				{
					$strClass = 'bx_item_detail_size';
					$strWidth = '100%';
					$strOneWidth = '20%';
					$strSlideStyle = 'display: none;';
				}
				$templateRow .= '<div class="'.$strClass.'" id="#ITEM#_prop_'.$arProp['ID'].'_cont">'.
'<span class="bx_item_section_name_gray">'.htmlspecialcharsex($arProp['NAME']).'</span>'.
'<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_'.$arProp['ID'].'_list" style="width: '.$strWidth.';">';
				foreach ($arProp['VALUES'] as $arOneValue)
				{
					$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
					$templateRow .= '<li data-treevalue="'.$arProp['ID'].'_'.$arOneValue['ID'].'" data-onevalue="'.$arOneValue['ID'].'" style="width: '.$strOneWidth.';" title="'.$arOneValue['NAME'].'"><i></i><span class="cnt">'.$arOneValue['NAME'].'</span></li>';
				}
				$templateRow .= '</ul></div>'.
'<div class="bx_slide_left" id="#ITEM#_prop_'.$arProp['ID'].'_left" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
'<div class="bx_slide_right" id="#ITEM#_prop_'.$arProp['ID'].'_right" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
'</div></div>';
			}
			elseif ('PICT' == $arProp['SHOW_MODE'])
			{
				if (5 < $arProp['VALUES_COUNT'])
				{
					$strClass = 'bx_item_detail_scu full';
					$strWidth = ($arProp['VALUES_COUNT']*20).'%';
					$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
					$strSlideStyle = '';
				}
				else
				{
					$strClass = 'bx_item_detail_scu';
					$strWidth = '100%';
					$strOneWidth = '20%';
					$strSlideStyle = 'display: none;';
				}
				$templateRow .= '<div class="'.$strClass.'" id="#ITEM#_prop_'.$arProp['ID'].'_cont">'.
'<span class="bx_item_section_name_gray">'.htmlspecialcharsex($arProp['NAME']).'</span>'.
'<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_'.$arProp['ID'].'_list" style="width: '.$strWidth.';">';
				foreach ($arProp['VALUES'] as $arOneValue)
				{
					$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
					$templateRow .= '<li data-treevalue="'.$arProp['ID'].'_'.$arOneValue['ID'].'" data-onevalue="'.$arOneValue['ID'].'" style="width: '.$strOneWidth.'; padding-top: '.$strOneWidth.';"><i title="'.$arOneValue['NAME'].'"></i>'.
'<span class="cnt"><span class="cnt_item" style="background-image:url(\''.$arOneValue['PICT']['SRC'].'\');" title="'.$arOneValue['NAME'].'"></span></span></li>';
				}
				$templateRow .= '</ul></div>'.
'<div class="bx_slide_left" id="#ITEM#_prop_'.$arProp['ID'].'_left" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
'<div class="bx_slide_right" id="#ITEM#_prop_'.$arProp['ID'].'_right" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
'</div></div>';
			}
			$arSkuTemplate[$arProp['CODE']] = $templateRow;
		}
		unset($templateRow, $arProp);
	}

	if ($arParams["DISPLAY_TOP_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}

	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?>
<div class="bx-section-desc <? echo $templateData['TEMPLATE_CLASS']; ?>">
</div>
<div id="waJewCatalog" class="catalog_block">
<?if (isset($_GET["arrFilter_132_3075670074"])):?>
<?
$APPLICATION->AddHeadScript($templateFolder."/js/jquery-1.11.1.min.js");
$APPLICATION->AddHeadScript($templateFolder."/js/jquery-migrate-1.2.1.min.js");
$APPLICATION->AddHeadScript($templateFolder."/js/slick.js");
$APPLICATION->SetAdditionalCSS($templateFolder."/js/slick.css");
				$arFilter = array("IBLOCK_ID" => 12);
				$arSelect = array("ID","IBLOCK_ID","NAME","PREVIEW_PICTURE","PROPERTY_JM_LINK", "PREVIEW_TEXT");
				$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);
				$bb = -1;
					while($ob = $res->GetNextElement())
						{
							$bb++;
							$arFields = $ob->GetFields();
							$actions[$bb]["ID"] = $arFields["ID"];
							$actions[$bb]["NAME"] = $arFields["NAME"];
							$pic = CFile::GetFileArray($arFields["PREVIEW_PICTURE"]);
							$actions[$bb]["PREVIEW_PICTURE"] = $pic["SRC"];
							$actions[$bb]["LINK"] = $arFields["PROPERTY_JM_LINK_VALUE"];
							$actions[$bb]["PREVIEW_TEXT"] = $arFields["PREVIEW_TEXT"];
						}	
?>
<script type="text/javascript">
$(document).ready(function() {
    $('.panel_big').slick({
	  autoplay: true,
        dots: true,
        infinite: true,
        speed: 800,
		autoplaySpeed: 8000,
        slidesToShow: 1,
        slidesToScroll: 1,
		cssEase: 'ease-in-out',
		arrows: false
    });
});	
</script>
<div class="action_slider">
<div class="panel_big">
<?foreach ($actions as $item):?>
	<div style="background-image:url(<?=$item["PREVIEW_PICTURE"];?>)">
	<div class="label">
				<a href="<?=$item["LINK"];?>"><?echo $item["NAME"];?></a>
			<span><?echo $item["PREVIEW_TEXT"];?></span>
	</div>
	</div>
<?endforeach;?>
</div>
</div>
<?endif?>
<?$des=-1;?>
	<?
foreach ($arResult['ITEMS'] as $key => $arItem)
{ 
//print_r($arItem["JEW_OFFER"]);
	/*	$product_arr = $arItem["ID"];
		$komplect_flag = false;
		$arKomplekt = array();
		$cc = -1;
		$all_offers = '';
		if ($arItem["DISPLAY_PROPERTIES"]["KOMPLEKT"]["DISPLAY_VALUE"] != '')
			{
				$komplekt_flag = true;
				$komplekt = $arItem["DISPLAY_PROPERTIES"]["KOMPLEKT"]["DISPLAY_VALUE"];
				$arFilter = array("PROPERTY_KOMPLEKT_VALUE" => $komplekt);
				$arSelect = array("ID","IBLOCK_ID","NAME","DETAIL_PICTURE","PROPERTY_CML2_ARTICLE","DETAIL_PAGE_URL");
				$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);
					while($ob = $res->GetNextElement())
						{
							$arFields = $ob->GetFields();
							if ($arFields["ID"] != $product_arr)
								{
							$cc++;
							$arKomplekt[$cc]["COUNT"] = $cc;
							$arKomplekt[$cc]["ID"] = $arFields["ID"];
							$arKomplekt[$cc]["NAME"] = $arFields["NAME"];
							$pic = CFile::GetFileArray($arFields["DETAIL_PICTURE"]);
							$arKomplekt[$cc]["DETAIL_PICTURE"] = $pic["SRC"];
							$arKomplekt[$cc]["ARTICLE"] = $arFields["PROPERTY_CML2_ARTICLE_VALUE"];
							$arKomplekt[$cc]["DETAIL_PAGE_URL"] = $arFields["DETAIL_PAGE_URL"];
								}
						}	
			$komp = -1;
			$all_stone_komp = array();
			$all_size_komp = array();
			$offers_komp = array();
			foreach ($arKomplekt as $value)
				{
				$komp = $value["COUNT"];
				$res = CCatalogSKU::getOffersList($value["ID"],0,array(),array("ID","CODE"),array());
							$stone = array();
							$size = array();
						$i = -1;
							$count1 = 0;						
						foreach($res[$value["ID"]] as $key1 => $elem)
						{
							$count1++;
							$arFilter = array("ID" => $key1);
							$arSelect = array("ID","PROPERTY_VSTAVKA","PROPERTY_RAZMER","CATALOG_GROUP_1");

							$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);

							while($ob = $res->GetNextElement())
							{
								$i++;
								
								$arFields = $ob->GetFields();
								//echo $arFields["ID"];
								$res_price = GetCatalogProductPrice($arFields["ID"],1);
								$offers_komp[$komp][$i]["SIZE"] = $arFields["PROPERTY_RAZMER_VALUE"];
								$offers_komp[$komp][$i]["STONE"] = $arFields["PROPERTY_VSTAVKA_VALUE"];
								$offers_komp[$komp][$i]["ID"] = $arFields["ID"];
								$offers_komp[$komp][$i]["PRICE"] = $res_price["PRICE"];
								$all_offers = $all_offers.$arFields["ID"].'-';
								if (array_search($arFields["PROPERTY_RAZMER_VALUE"],$all_size) == false)
									{
										$all_size_komp[$komp][] = $arFields["PROPERTY_RAZMER_VALUE"];
									}
								if (array_search($arFields["PROPERTY_VSTAVKA_VALUE"],$all_stone) == false)
									{
										$all_stone_komp[$komp][] = $arFields["PROPERTY_VSTAVKA_VALUE"];
									}

							}
						}
							$all_size_komp[$komp] = array_unique($all_size_komp[$komp]);
							$all_stone_komp[$komp] = array_unique($all_stone_komp[$komp]);		
				}
			}

		$res = CCatalogSKU::getOffersList($product_arr,0,array(),array("ID","CODE"),array());
					$stone = array();
					$size = array();
					$all_stone = array();
					$all_size = array();
					$offers = array();
				$i = -1;
					$count1 = 0;

				foreach($res[$arItem["ID"]] as $key1 => $elem)
				{
					
					$count1++;
					$arFilter = array("ID" => $key1);
					$arSelect = array("ID","PROPERTY_VSTAVKA","PROPERTY_RAZMER"," CATALOG_GROUP_1");

					$res =  CIBlockElement :: GetList (array(), $arFilter, false,false, $arSelect);

					while($ob = $res->GetNextElement())
					{
						$i++;
						$arFields = $ob->GetFields();
						$res_price = GetCatalogProductPrice($arFields["ID"],1);
						$offers[$i]["SIZE"] = $arFields["PROPERTY_RAZMER_VALUE"];
						$offers[$i]["STONE"] = $arFields["PROPERTY_VSTAVKA_VALUE"];
						$offers[$i]["ID"] = $arFields["ID"];
						$offers[$i]["PRICE"] = $res_price["PRICE"];
						$all_offers = $all_offers.$arFields["ID"].'-';
						if (array_search($arFields["PROPERTY_RAZMER_VALUE"],$all_size) == false)
							{
								$all_size[] = $arFields["PROPERTY_RAZMER_VALUE"];
							}
						if (array_search($arFields["PROPERTY_VSTAVKA_VALUE"],$all_stone) == false)
							{
								$all_stone[] = $arFields["PROPERTY_VSTAVKA_VALUE"];
							}

					}
				}
				
					$all_size = array_unique($all_size);
					$all_stone = array_unique($all_stone);*/
					
	
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($arItem['ID']);

	$arItemIDs = array(
		'ID' => $strMainID,
		'PICT' => $strMainID.'_pict',
		'SECOND_PICT' => $strMainID.'_secondpict',
		'STICKER_ID' => $strMainID.'_sticker',
		'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
		'QUANTITY' => $strMainID.'_quantity',
		'QUANTITY_DOWN' => $strMainID.'_quant_down',
		'QUANTITY_UP' => $strMainID.'_quant_up',
		'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
		'BUY_LINK' => $strMainID.'_buy_link',
		'BASKET_ACTIONS' => $strMainID.'_basket_actions',
		'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
		'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
		'COMPARE_LINK' => $strMainID.'_compare_link',

		'PRICE' => $strMainID.'_price',
		'DSC_PERC' => $strMainID.'_dsc_perc',
		'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
		'PROP_DIV' => $strMainID.'_sku_tree',
		'PROP' => $strMainID.'_prop_',
		'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
		'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
	);

	$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

	$productTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $arItem['NAME']
	);
	$imgTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
		: $arItem['NAME']
	);

	$minPrice = false;
	if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
		$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
	?>

	
<div class="catalog_item">

			<a class="a_pic" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" style="background-image: url(<?
			if ($arItem["DETAIL_PICTURE"]["SRC"] != '')
			{
				echo $arItem["DETAIL_PICTURE"]["SRC"];
			}
			else
			{
				echo $arItem['PREVIEW_PICTURE']['SRC'];
			}			
			?>)"></a>
			<div class="title"><a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><? echo $productTitle; ?></a></div>
<?
$articul = $arItem["DISPLAY_PROPERTIES"]["CML2_ARTICLE"]["DISPLAY_VALUE"];
unset($arItem["DISPLAY_PROPERTIES"]["CML2_ARTICLE"]);
unset($arItem["DISPLAY_PROPERTIES"]["KOMPLEKT"]);
$articul = explode('-',$articul);
$num = $articul[1];			
?>
			<div class="articul">Артикул: <?=$num?></div>
			<div class="price">
				<?
				//echo $res_price["PRICE"];
	if (!empty($minPrice))
	{
		if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
		{
			echo GetMessage(
				'CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE',
				array(
					'#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE'],
					'#MEASURE#' => GetMessage(
						'CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE',
						array(
							'#VALUE#' => $minPrice['CATALOG_MEASURE_RATIO'],
							'#UNIT#' => $minPrice['CATALOG_MEASURE_NAME']
						)
					)
				)
			);
		}
		else
		{
			echo $minPrice['PRINT_DISCOUNT_VALUE'];
		}
		if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
		{
			?> <span><? echo $minPrice['PRINT_VALUE']; ?></span><?
		}
	}
	unset($minPrice);				
				?> руб<!--<span></span> -->
				
			</div>
			<div class="hidden">
				<div class="hidden_block prev_pic">
					<div class="cross"></div>
					<img src="<?
			if ($arItem["DETAIL_PICTURE"]["SRC"] != '')
			{
				echo $arItem["DETAIL_PICTURE"]["SRC"];
			}
			else
			{
				echo $arItem['PREVIEW_PICTURE']['SRC'];
			}			
			?>">					
				</div>
			</div>
			<span title="Увеличить изображение. Если не работает - перезагрузите страницу" class="image_plus"><i class="fa fa-search-plus fa-fw"></i></span>
				<form class="scupanel">
<?
// Начало вывода свойств товара
					if (isset($arItem['DISPLAY_PROPERTIES']) && !empty($arItem['DISPLAY_PROPERTIES']))
					{
					?>
						<div class="bx_catalog_item_articul">
					<?
						foreach ($arItem['DISPLAY_PROPERTIES'] as $arOneProp)
						{
							?><br><strong><? echo $arOneProp['NAME']; ?></strong> <?
								echo (
									is_array($arOneProp['DISPLAY_VALUE'])
									? implode('<br>', $arOneProp['DISPLAY_VALUE'])
									: $arOneProp['DISPLAY_VALUE']
								);
						}
					?><br>
						</div>
					<?
					}
// Конец вывода свойств товара		
?>
						<?$first = $arItem["JEW_INSERT"][0];?>
						<div class="scusize" id="wainsert">
						<div class="inp"><?=$first?></div>
						<span><i class="fa fa-sort-desc fa-fw" aria-hidden="true"></i></span>
							<ul>
								<?foreach($arItem["JEW_INSERT"] as $key => $item):?>
									<li jew-key="<?echo 'wa_insert_'.$key;?>"><?=$item;?></li>
								<?endforeach;?>
							</ul>
						</div>
						<?$first = $arItem["JEW_OFFER"][0];?>
						<div class="scusize" id="wasize">
						<div class="inp"><?=$first["SIZE"]?></div>
						<span><i class="fa fa-sort-desc fa-fw" aria-hidden="true"></i></span>
							<ul>
								<?foreach($arItem["JEW_OFFER"] as $key => $item):?>
									<?$class = ($item["WAID"] > 0) ? 'wascuhidd' : '';?>
									<li class="<?=$class;?>" jew-value="<?=$item["ID"]?>" jew-key="<?echo 'wa_insert_'.$item["WAID"];?>"><?=$item["SIZE"];?></li>
								<?endforeach?>
							</ul>
						</div>					
				
				
				
			

				<input type="hidden" name="offers" value="<?=$first["ID"]?>">
				<div id="waScuBuy" class="scubuy">Купить</div>
				</form>
</div>
<?
/*		echo '<pre>';
		print_r($arItem["JEW_OFFER"]);
		echo '</pre>';*/
?>
<?
}
/*}*/
?>
</div>
<!-- </div> -->
<div class="washadow">
	<div class="wabuywindow">
		Товар добавлен в корзину
	</div>
</div>
<script type="text/javascript">
BX.message({
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
	BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
	ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});
</script>
<?
	if ($arParams["DISPLAY_BOTTOM_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
}
?>
<script>
	$('body').on('mouseenter','.catalog_item', function(){
		$('.catalog_item .scupanel').hide();
		$(this).find('.scupanel').fadeToggle(500);
	});
	$('body').on('mouseleave','.catalog_item', function(){
		$('.catalog_item .scusize ul').hide();
		$('.catalog_item .scupanel ul').hide();
		$(this).find("#waScuBuy").text("Купить");
		$(this).find("#waScuBuy").removeClass("wagreen");
	});
	$('body').on('click','.scusize', function() {
		var ul = $(this).find('ul');
		ul.toggle();
	});
	$('body').on('mouseleave','.catalog_block', function(){
		$('.catalog_item .scupanel').hide();
	});
	$('body').on('mouseleave','.scusize ul', function(){
		$(this).hide();
//		$(this).find('.scupanel').fadeToggle(500);
	});
	$('body').on('click','.scusize ul li', function(){
		var text = $(this).text();		
		var id = $(this).attr('jew-value');
		$(this).parents('.scusize').children('.inp').text(text);
		$(this).parents('.scusize').next().attr('value',id);
	});
	$('body').on('click','#wainsert ul li', function(){
		var key = $(this).attr("jew-key");
		var scu = $(this).parents("#wainsert").next();
		var li = scu.find('li');
		var i = 0;
		li.removeClass("wascuhidd");
		li.each(function(i,e){
			if ($(this).attr('jew-key') != key)
			{
				$(this).addClass("wascuhidd");
			}
			else
			{
				i++;
				if (i = 1)
					{
						scu.next().attr('value',$(this).attr("jew-value"));
						scu.children('.inp').text($(this).text());
					}
			}
		});
	});
	$('body').on('click','#waScuBuy', function(){
			$(this).text('Товар добавлен');
			$(this).addClass("wagreen");
			var obj = $(this).parents('.scupanel').serialize();
			BX.ajax.post (
				"/jewcat/add2basket2.php",
				obj,
				returned5
			)
			function returned5(data){
				
			}		
	});
/*	$('body').on('click','.washadow', function(){
			$(this).hide();
	});*/
/*	$('body').on('mouseout','.catalog_item', function(){
	)*/
	/*    function AjaxFormRequest() {
				alert ('fgsdfg');
                $.ajax({
                    url:     "/jewcat/add2basket.php", //Адрес подгружаемой страницы
                    type:     "POST", //Тип запроса
                    dataType: "html", //Тип данных
                    data: jQuery("#add2basket").serialize(), 
                    success: function(response) { //Если все нормально
                    document.getElementById(result_id).innerHTML = response;
                },
                error: function(response) { //Если ошибка
                document.getElementById(result_id).innerHTML = "Ошибка при отправке формы";
                }
             });
        }*/
</script>
