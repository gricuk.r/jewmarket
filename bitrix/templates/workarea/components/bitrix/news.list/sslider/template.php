<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id="rowSlider" class="row">
        <div id="slider" class="col-xs-12 waBlock" style="padding: 0;">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<div class="waSlideFull" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)">
			</div>
		<?endforeach;?>
        </div>
</div>
