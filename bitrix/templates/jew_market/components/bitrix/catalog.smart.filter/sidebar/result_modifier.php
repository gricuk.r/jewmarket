<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (isset($arParams["TEMPLATE_THEME"]) && !empty($arParams["TEMPLATE_THEME"]))
{
	$arAvailableThemes = array();
	$dir = trim(preg_replace("'[\\\\/]+'", "/", dirname(__FILE__)."/themes/"));
	if (is_dir($dir) && $directory = opendir($dir))
	{
		while (($file = readdir($directory)) !== false)
		{
			if ($file != "." && $file != ".." && is_dir($dir.$file))
				$arAvailableThemes[] = $file;
		}
		closedir($directory);
	}

	if ($arParams["TEMPLATE_THEME"] == "site")
	{
		$solution = COption::GetOptionString("main", "wizard_solution", "", SITE_ID);
		if ($solution == "eshop")
		{
			$theme = COption::GetOptionString("main", "wizard_eshop_adapt_theme_id", "blue", SITE_ID);
			$arParams["TEMPLATE_THEME"] = (in_array($theme, $arAvailableThemes)) ? $theme : "blue";
		}
	}
	else
	{
		$arParams["TEMPLATE_THEME"] = (in_array($arParams["TEMPLATE_THEME"], $arAvailableThemes)) ? $arParams["TEMPLATE_THEME"] : "blue";
	}
}
else
{
	$arParams["TEMPLATE_THEME"] = "blue";
}
/*function array_swap(array &$array, $key, $key2)
{
    if (isset($array[$key]) && isset($array[$key2])) {
        list($array[$key], $array[$key2]) = array($array[$key2], $array[$key]);
        return true;
    }

    return false;
}
$fl1 = -1;
foreach($arResult["ITEMS"] as $key => $arItem)
{	
	$fl1++;
	if ($fl1 == 2 )
	{
		$waNewKey = $key;
	}
}
array_swap($arResult["ITEMS"],$waNewKey,488);*/
/*$fl1 = -1;
$waBuff = $arResult["ITEMS"][488];
foreach($arResult["ITEMS"] as $key => $arItem)
{	
	if ($fl1 == 2 )
	{
		$waBuffKey = $key;
	}
}
$waBuff2 = $arResult["ITEMS"][$waBuffKey];
echo '<pre>';
var_dump($waBuffKey);
echo '</pre>';
*/
/*echo '<pre>';
var_dump($waBuffKey);
echo '</pre>';/*

/*foreach($arResult["ITEMS"] as $key => $arItem)
{	
	if ($arItem["NAME"] == 'ВСТАВКА')
	{
		$waInsertKey["KEY"] = $key;
		$waInsertKey["VAL"] = $arItem;	
	}
	else
	{
		$waTestArr[$key] = $arItem;
	}
}*/
/*$fl = -1;
foreach($waTestArr as $key => $arItem)
{
	$fl++;
	if ($fl == 2)
	{
		
		$arResult2[$waInsertKey["KEY"]] = $waInsertKey["VAL"];
		}
	else
	{
		$arResult2[$key] = $arItem;
	}
}
$arResult["ITEMS"] = $arResult2;*/
//unset($arItem);
?>
