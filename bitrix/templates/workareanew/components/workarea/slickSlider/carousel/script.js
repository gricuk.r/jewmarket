function WASSlider (id, props, path) {
	this.id = id;
	this.path = path;	
	this.props = props;
	this.timer = null;
	this.stat = false;
	this.init();
}
WASSlider.prototype.init = function ()
{
	var self = this;
//	this.prepare();
	$(this.id).parent().addClass("notReady");
	if(!!this.timer)
	{
		clearTimeout(this.timer);
	}
	this.timer = setTimeout(BX.delegate(function(){
		this.reload(this);
	}, this), 500);	
}
WASSlider.prototype.reload = function ()
{
	var self = this;
	var test = $("#" + this.id.getAttribute("id")).getSlick;
//	console.dir();
	if (test)
	{
		var pp = this.props;
		pp["responsive"] = [{
			breakpoint: 500,
			settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		  }
		}];
		$("#" + this.id.getAttribute("id")).slick(pp);
//		$('.sizeSlider').slick();
	}
	else
	{
		BX.ajax.loadScriptAjax(
		this.path,
		 function callback () {
			// var _self = self;
			 //setTimeout( function () {_self.init()};
			 self.init();
//			 $("#" + this.id.getAttribute("id")).slick(this.props);
		 }

		);		
	}
	$(this.id).parent().removeClass("notReady");
}
/*WASSlider.prototype.prepare = function ()
{
	var self = this;
	var elem = $(this.id).parent();
	var stat = elem.toggleClass("waReady");
	
	};
	//this.stat;
	console.dir(this.stat);
	//if (status)
	//var status = $(self).attr("");

	
}*/
$('.sizeSlider').slick
console.dir(window);