<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalCss($templateFolder."/css/slick.css");
$this->addExternalCss($templateFolder."/css/slick-theme.css");
//$this->addExternalJS($templateFolder."js/jquery-migrate-1.2.1.min.js");
//$this->addExternalJS($templateFolder."js/slick.js");

?>
<?
//if ($USER->IsAdmin()) {echo "<pre>"; var_dump($this->parent); echo "</pre>";}

$templateData["TEMPLATE_CLASS"] = "WASS_".toUpper($arParams["WASS_PROPS"]["type"]);
//if ($USER->IsAdmin()) {echo "<pre>"; print_r($arParams["WASS_PROPS"]); echo "</pre>";}
?>
<div class="row">
	<div id="<?=$arParams["SLIDER_ID"]?>" class="col-xs-12 <?=$templateData["TEMPLATE_CLASS"]?>">

		<?foreach($arResult["ITEMS"] as $arItem):?>		
		<div class="item">
			<a class="image" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="img-responsive" />
				<div class="price"><?=$arItem["PRICE"]?></div>
				<?if (!empty($arItem["PROPERTIES"]["AKTSIYA"]["VALUE"]) || !empty($arItem["PROPERTIES"]["NOVINKA"]["VALUE"])):?>
				<?$i++?>
					<span class="action" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/images/snew<?=$i;?>.png);"></span>
				<?endif?>
			</a>
			<?if ($arParams["SHOW_DESCRIPTION"] != "N"):?>
				<div class="title"><a href="#"><?=$arItem["NAME"]?></a></div>	
				<?if (!empty($arResult["DISPLAY_PROPERTIES"])):?>
				<div class="description">
					<?foreach($arResult["DISPLAY_PROPERTIES"] as $key => $value):?>
						<div class="property"><?=$key?> : <?=$value?></div>
					<?endforeach;?>
				</div>
				<?endif;?>
			<?endif?>
		</div>	
		<?endforeach;?>
	</div>
</div>
<script>
var sliderOn = new WASSlider(<?echo CUtil::JSEscape($arParams["SLIDER_ID"])?>, <?=CUtil::PhpToJSObject($arParams["WASS_PROPS"])?>, "<?=$templateFolder."/js/slick.js"?>");
//window.addEventListener("hashchange", function() {}, false);

//extend(WASSlider,Slick);
/*$(document).ready( function () {
	this.jq = $(this);
	var self = $(this);
	var sliderOn = new WASSlider(<?echo CUtil::JSEscape($arParams["SLIDER_ID"])?>, <?=CUtil::PhpToJSObject($arParams["WASS_PROPS"])?>);
	BX.addCustomEvent('onAjaxSuccess', function () {
		BX.delegate(function () {
			this.sliderOn(self);
		},self);
	});
});*/
/*BX.addCustomEvent('onAjaxSuccess', function(){
var sliderOn = new WASSlider(<?echo CUtil::JSEscape($arParams["SLIDER_ID"])?>, <?=CUtil::PhpToJSObject($arParams["WASS_PROPS"])?>);
});*/
/*var SliderOn = function ()
{
	var params = <?=CUtil::PhpToJSObject($arParams["WASS_PROPS"])?>;
	$("#<?echo CUtil::JSEscape($arParams["SLIDER_ID"])?>").slick(params);	
	BX.removeCustomEvent('onAjaxSuccess', SliderOn);
}	
BX.addCustomEvent('onAjaxSuccess', SliderOn);*/

		


$(document).ready( function () {
//	var slider = new WASSlider(<?echo CUtil::JSEscape($arParams["SLIDER_ID"])?>, <?=CUtil::PhpToJSObject($arParams["WASS_PROPS"])?>);
	//	$(this).getSlick();
		//console.dir(window.$);
});
/*BX.addCustomEvent('onAjaxSuccess', function(){
	console.dir(event);

	BX.ajax.loadScriptAjax(
	'<?=$templateFolder."/js/slick.js"?>',
	 function callback () {
var params = <?=CUtil::PhpToJSObject($arParams["WASS_PROPS"])?>;
					$("#<?echo CUtil::JSEscape($arParams["SLIDER_ID"])?>").slick(params);
	 }

	);
	
});*/
 /*       BX.addCustomEvent('onAjaxSuccess', function(){
		window.slick("sdf");	
			var success = this.response;
			//success = this.response;
			console.dir(BX(success));	
		if (this.response.STATUS == "SUCCESS")
		{
		
			if(!!this.timer)
			{
				clearTimeout(this.timer);
			}
			else
			{
				this.timer = window.setTimeout( function ($) {	
					var params = <?=CUtil::PhpToJSObject($arParams["WASS_PROPS"])?>;
					$("#<?echo CUtil::JSEscape($arParams["SLIDER_ID"])?>").slick(params);
				}, 3500);
			}
		}
		});*/

/*setTimeout( function () {

	var params = <?=CUtil::PhpToJSObject($arParams["WASS_PROPS"])?>;
	console.dir(params);
	$("#<?echo CUtil::JSEscape($arParams["SLIDER_ID"])?>").slick(
	params
);
//var detail_<?=$arParams["SLIDER_ID"]?> = new WASSlider(<?echo CUtil::JSEscape($arParams["SLIDER_ID"])?>, <?=CUtil::PhpToJSObject($arParams["WASS_PROPS"])?>);
}, 3500);*/
/*BX.ready(function () {
	var params = <?=CUtil::PhpToJSObject($arParams["WASS_PROPS"])?>;
	console.dir(params);
	$("#<?echo CUtil::JSEscape($arParams["SLIDER_ID"])?>").slick(
	params
);
}*/
</script>