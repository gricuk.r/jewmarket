<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$i=0;
?>
<div id="rowCarousel2" class="row">
	<div id="waCarousel2" class="col-xs-12 waBlock waItems">

		<?foreach($arResult["ITEMS"] as $arItem):?>		
		<div class="waSlide" style="padding: 0">
			<a class="rel" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="img-responsive" />
				<div class="price"><?=$arItem["PRICE"]?></div>
				<?if (!empty($arItem["PROPERTIES"]["AKTSIYA"]["VALUE"]) || !empty($arItem["PROPERTIES"]["NOVINKA"]["VALUE"])):?>
				<?$i++?>
					<span class="snew" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/images/snew<?=$i;?>.png);"></span>
				<?endif?>
			</a>
			<div class="title"><a href="#"><?=$arItem["NAME"]?></a></div>	
			<div class="article">Артикул: <?=$arItem["ARTICLE"]?></div>	
		</div>	
		<?endforeach;?>
	</div>
</div>
