<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id="rowSlider">
        <div id="slider"  style="padding-right: 0px padding-left: 0px;">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<div class="waSlideFull" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)">
			</div>
		<?endforeach;?>
        </div>
</div>
