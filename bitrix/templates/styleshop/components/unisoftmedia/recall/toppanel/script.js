;(function($, window, document, undefined) {
'use strict';

	if (!!window.Recall)
	{
		return;
	}

	function Recall()
	{
		this.init();
	}

	Recall.prototype = {

		init: function()
		{
			$(document).on('click', '.ajax-recall', function(e){
				e.preventDefault();

				var data = {
					'form_popup': $(this).data('form-popup')
				};

				$.fancybox({
					type: 'ajax', 
					autoSize: true,
					width: 500,
					maxWidth: 500,
					minWidth: 230,
					cache: false,
					ajax: {
						dataType: 'html',
						headers: {
							'X-fancyBox': true
						},
						data: data,
					},
					helpers: {
						title: {
							type: 'inside',
							position: 'top'
						}
					},
					title: $(this).data('form-title'),
					href : $(this).data('form-action'), 
					openEffect	: 'fade',
					closeEffect	: 'fade'
				});
			});
		},

		submit: function(id)
		{
			var L = new Loader,
					$element = $('#'+id),
					$form = ($element.is('form'))? $element : $element.find('form');

			L.add($element);
			$.ajax({
				type: $form.attr('method'),
				url: $form.attr('action'),
				data: $form.serialize(),
			}).done(function(response) {
				L.end();
				$element.html(response);
			}).fail(function(response) {
				L.end();
				console.log('error');
			});
		}
	};

	window.Recall = new Recall();

})( window.jQuery, window, document );